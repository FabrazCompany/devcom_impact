﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPosition : MonoBehaviour
{
Vector3 defaultPos;
 void Start(){
     defaultPos = transform.position;
 }
public void Reset (){ 
 transform.position = defaultPos;
}
}