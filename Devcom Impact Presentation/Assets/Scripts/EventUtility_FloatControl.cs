using UnityEngine;

public class EventUtility_FloatControl : MonoBehaviour
{
    [SerializeField] FloatDependentEvent[] floatEvents;
    public void OnTriggerFloatEvent (float val) 
    {
        for (int i = 0; i < floatEvents.Length; i++) 
        {
            floatEvents[i].Trigger (val);
        }
    }

    [System.Serializable]
    class FloatDependentEvent
    {
        [SerializeField] float minValue = 0;
        [SerializeField] float maxValue = 1;
        [SerializeField] UnityEventFloat events;

        public void Trigger (float val) 
        {
            events?.Invoke (Mathf.Lerp (minValue, maxValue, val));
        }
    }
}