﻿using UnityEngine;
using UnityEngine.Events;
public class Timer : MonoBehaviour 
{
	[SerializeField] float duration;
	[SerializeField] bool ignoreTimeScale;
	[SerializeField] bool repeat;
	[SerializeField, Tooltip ("If true, progress event will decrease to 0, rather than increase to 1")] bool subtractiveProgress;

	[SerializeField] UnityEvent onTimerFinished;
	[SerializeField] UnityEventFloat onTimerUpdated;

	float timer;

	void OnEnable() 
	{
		timer = 0;
	}

	void Update () {
		timer += ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime;
		var pct = timer / duration;
		if (subtractiveProgress)
			pct = 1 - pct;

		onTimerUpdated?.Invoke (pct);
		if (timer >= duration) 
		{
			if (repeat) 
			{
				timer = 0;
			} 
			else 
			{
				enabled = false;
			}
			onTimerFinished.Invoke();	
		}
	}

	public void SetTimer (float newDur, bool shouldEnable = true) 
	{
		timer = 0;
		duration = newDur;
		enabled = shouldEnable;
	}
}
