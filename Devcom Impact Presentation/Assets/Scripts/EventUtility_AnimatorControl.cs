using UnityEngine;
#if UNITY_EDITOR
using UnityEditor.Animations;
using UnityEditor;
#endif

[RequireComponent(typeof(Animator))]
public class EventUtility_AnimatorControl : MonoBehaviour {
	Animator anim;
	void Awake () {
		anim = GetComponent<Animator> ();
	}

	public void ParameterBoolEnable (string id) {
		anim.SetBool (id, true);
	}
	public void ParameterBoolDisable (string id) {
		anim.SetBool (id, false);
	}
	public void ParameterFloatZero (string id) {
		anim.SetFloat (id, 0);
	}
	public void ParameterFloatOne (string id) {
		anim.SetFloat (id, 1);
	}

	[SerializeField] string targetID;
	public void SetTargetID (string val) {
		targetID = val;
	}
	public void ParameterInt (int val) {
		anim.SetInteger (targetID, val);
	}
	public void ParameterFloat (float val) {
		anim.SetFloat (targetID, val);
	}
	public void ParameterBool (bool val) {
		anim.SetBool (targetID, val);
	}

#if UNITY_EDITOR
 
    [MenuItem("AnimTools/GameObject/Asset from Blendtree")]
    static void CreateBlendtree()
    {
        BlendTree BT = Selection.activeObject as BlendTree;
 
        BlendTree BTcopy = Instantiate<BlendTree>(BT);

		// AssetDatabase.fol
		// string path = ProjectUtility.GetCurrentProjectPath ();
		string path = string.Empty;
        AssetDatabase.CreateAsset(BTcopy, AssetDatabase.GenerateUniqueAssetPath(path + BT.name + ".asset"));
    }
#endif

}
