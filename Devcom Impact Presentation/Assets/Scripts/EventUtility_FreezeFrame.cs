using System.Collections;
using UnityEngine;

public class EventUtility_FreezeFrame : MonoBehaviour
{
	public void TriggerFreezeFrame (float duration) 
	{
		StartCoroutine (FreezeFrameRoutine (duration));
	}
	IEnumerator FreezeFrameRoutine (float duration) 
	{
		Time.timeScale = 0;
		yield return new WaitForSecondsRealtime (duration);
		Time.timeScale = 1;
	}
}