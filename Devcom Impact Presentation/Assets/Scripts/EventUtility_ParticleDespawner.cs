﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventUtility_ParticleDespawner : MonoBehaviour
{
    [SerializeField] GameObject parent;
    
    
    public void OnParticleSystemStopped () 
    {
        Destroy (parent);
    }
}
