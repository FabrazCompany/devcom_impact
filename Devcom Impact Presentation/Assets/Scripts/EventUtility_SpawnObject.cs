using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventUtility_SpawnObject : MonoBehaviour {
    public void SpawnObject (GameObject _object) 
    {
        TrashMan.spawn (_object, transform.position);
    }
}
