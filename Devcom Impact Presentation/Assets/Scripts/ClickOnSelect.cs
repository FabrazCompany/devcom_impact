using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent (typeof (Button))]
public class ClickOnSelect : MonoBehaviour, ISelectHandler
{
    
    void ISelectHandler.OnSelect(BaseEventData eventData)
    {
        Button butt = GetComponent<Button> ();
        butt.OnPointerClick (new PointerEventData (EventSystem.current));
    }
}