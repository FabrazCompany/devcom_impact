﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Presentation : MonoBehaviour
{
    [SerializeField] Camera presiUICamera;
    [SerializeField] GameObject presiControls;
    void Start()
    {
        Time.timeScale = 0;


    }

    public void TriggerPresi () 
    {
        Time.timeScale = 1;
        presiUICamera.gameObject.SetActive (true);
        presiControls.SetActive (true);
    }

    public void EndPresi () 
    {
        Time.timeScale = 0;
        presiUICamera.gameObject.SetActive (false);
        presiControls.SetActive (false);
    }
}
