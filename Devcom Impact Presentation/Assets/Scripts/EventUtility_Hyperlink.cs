using UnityEngine;

public class EventUtility_Hyperlink : MonoBehaviour
{

    // https://bitbucket.org/FabrazCompany/devcom_impact/
    public void OpenLink (string link)
    {
        // WWW.
        Application.OpenURL (link);
    }
}