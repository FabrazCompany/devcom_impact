using UnityEngine;
using UnityEngine.Events;


[System.Serializable]
public class UnityEventBool : UnityEvent<bool> {
}

[System.Serializable]
public class UnityEventFloat : UnityEvent<float> {

}
[System.Serializable]
public class UnityEventInt : UnityEvent<int> {

}
[System.Serializable]
public class UnityEventTransform : UnityEvent<Transform> {

}

[System.Serializable]
public class UnityEventCollider : UnityEvent<Collider> {

}
[System.Serializable]
public class UnityEventCollision : UnityEvent<Collision> {

}
[System.Serializable]
public class UnityEventColliderAndPosition : UnityEvent<Collider,Vector3> {

}
[System.Serializable]
public class UnityEventVector3 : UnityEvent<Vector3> {

}
