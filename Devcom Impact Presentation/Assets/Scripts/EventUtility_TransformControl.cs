﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventUtility_TransformControl : MonoBehaviour
{
    public void ZeroOutChildRotation () {
        if (transform.parent == null)
            return;
        transform.localRotation = Quaternion.identity;
    }
    public void ZeroOutChildPosition () {
        if (transform.parent == null)
            return;
        transform.localPosition = Vector3.zero;
    }
}
