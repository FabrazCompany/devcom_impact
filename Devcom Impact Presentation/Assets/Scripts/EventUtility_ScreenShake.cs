﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EventUtility_ScreenShake : MonoBehaviour
{
	[SerializeField] float magnitude = 1;
	[SerializeField] int vibrato = 10;
	[SerializeField] float duration = .25f; 
	public void TriggerScreenShake () 
	{
		transform.DOShakePosition (duration, magnitude, vibrato);
	}
}
