﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class CameraEffectsController : MonoBehaviour
{
    PostProcessVolume volume;
    void Awake () 
    {
        volume = GetComponent<PostProcessVolume> ();
    }
    public void SetProcessing (bool active) 
    {
        var profile = volume.profile;
        
        var motionBlur = profile.GetSetting<MotionBlur> ();
        if (motionBlur)
        {
            motionBlur.enabled.value = active;
            // motionBlur.enabled.overrideState = active;
            // motionBlur.active = active;
            // motionBlur.enabled.value = active;
            // motionBlur.
            Debug.Log ("Setting!");
            
        }
            
    }
}
