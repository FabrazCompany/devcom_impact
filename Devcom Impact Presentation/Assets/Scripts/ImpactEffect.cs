﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[RequireComponent (typeof (Rigidbody))]
public class ImpactEffect : MonoBehaviour 
{
	bool isVisible;
	void OnBecameVisible () => isVisible = true;
	void OnBecameInvisible () => isVisible = false;
	[SerializeField] UnityEvent onImpact;
	Rigidbody rigid;
	void Awake () 
	{
		rigid = GetComponent<Rigidbody> ();
	}
	void OnCollisionEnter(Collision hit) 
	{
		if (!isVisible)
			return;
		onImpact.Invoke();
	}
}
